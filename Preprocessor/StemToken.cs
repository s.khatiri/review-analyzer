﻿using System.Collections.Generic;
using System.Linq;

namespace Preprocessor
{
    public class StemToken
    {
        public string Token;
        public List<string> Stems;
        public bool StopWord = false;
        public bool SpellError;
        public string Language;

        public StemToken(string token, List<string> stems, string lang, bool spellError)
        {
            Token = token;
            Stems = stems;
            SpellError = spellError;
            Language = lang;
        }
        public StemToken(string token, string lang)
        {
            Token = token;
            Stems = new List<string>();
            SpellError = true;
            Language = lang;
        }
        public StemToken(string token)
        {
            Token = token;
            Stems = new List<string>();
            SpellError = false;
            Language = "NA";
        }

        public static IEnumerable<string> ToStemList(IEnumerable<StemToken> tokens)
        {
            return tokens.SelectMany(x => x.Stems);
            //var stems=new List<string>();
            //tokens.ForEach(x=>stems.AddRange(x.Stems));
            //return stems;
        }
    }
}
