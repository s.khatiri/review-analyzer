﻿using Preprocessor;
using System.Collections.Generic;
using System.Linq;

namespace Preprocessor
{
    public static class CharacterNormalizer
    {
        public static string Normalize(string srcStr, bool preserveConsecutive = false)
        {

            var desStr = new string(ReplaceSequences(srcStr)
                .Select(x => OmitRedundant(x)).Where(x => x != default(char))
                .ToArray());
            return desStr;
        }

        private static char OmitRedundant(char srcChar)
        {

            if (Helper.IsEnglish(srcChar))
                return char.ToLower(srcChar);

            if (Helper.IsDigit(srcChar)) //digits
                return srcChar;

            if (IsAllowedSymbol(srcChar))
                return srcChar;

            return ' ';
        }

        private static string ReplaceSequences(string srcStr)
        {
            if (string.IsNullOrEmpty(srcStr)) return "";
            var usrnm = Helper.RemoveUserNames(srcStr);
            return usrnm;
        }


        private static bool IsAllowedSymbol(char srcChar)
        {
            if (srcChar == '.' || srcChar == '@' || srcChar == '/' || srcChar == ':' || srcChar == '&' || srcChar == '%' ||
                srcChar == '#' || srcChar == '!' || srcChar == '~' || srcChar == '=' || srcChar == '_' || srcChar == '-' || srcChar == '?' ||
                srcChar == '\n' || srcChar == '\t' || srcChar == '\\')
                return true;
            return false;
        }

        public static string OmitConsecutive(string srcStr)
        {
            var desStr = "";
            for (int i = 0; i < srcStr.Length - 1; i++)
            {
                var last = desStr.LastOrDefault();
                if (last != srcStr[i] || srcStr[i] != srcStr[i + 1] || (last.ToString().ToLower() == "w" && last == srcStr[i] && srcStr[i] == srcStr[i + 1]))
                {
                    if (!Helper.IsInUrl(srcStr, i) && Helper.IsSpaceNeededInBetween(last, srcStr[i]))
                    {
                        desStr += ' ';
                    }
                    desStr += srcStr[i];
                }
                else    //consecutive digits are not omited
                {
                    if (Helper.IsDigit(srcStr[i]))
                    {
                        if (last != default(char) && (!Helper.IsInUrl(srcStr, i)) && (!Helper.IsDigit(last)))
                        {
                            desStr += ' ';
                        }
                        desStr += srcStr[i];
                    }
                }
            }
            desStr += srcStr.LastOrDefault();
            return desStr;

        }


        public static List<string> OmitConsecutiveToken(string token, bool preserve)
        {
            if (preserve)
            {
                return new List<string>() { token };
            }
            var desStr = "";
            for (int i = 0; i < token.Length; i++)
            {
                var last = desStr.LastOrDefault();
                if ((i < token.Length - 1) && (last.ToString() + token[i] + token[i + 1]).ToLower() == "www")
                {
                    desStr += token[i];
                }
                else
                {
                    if (Helper.IsDigit(token[i])) //consecutive digits are not omited
                    {
                        desStr += token[i];
                    }
                    else if (last != token[i])
                    {
                        desStr += token[i];
                    }
                }
            }
            if (token != desStr)
            {
                return new List<string>() { token, desStr };
            }
            return new List<string>() { token };
        }
    }
}
