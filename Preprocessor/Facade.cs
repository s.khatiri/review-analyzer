﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Preprocessor
{
    public class Facade
    {
        private Stemmer _stemmer;
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        public static double MaxEnglishSpellErrorRate=0.4;
        public static int MinInformativeDistinctWords = 5;
        public List<List<string>> Process(IEnumerable<string> texts)
        {
            return texts.AsParallel().AsOrdered().Select(x => new List<string> { x, string.Join(" ", Process(x, false).Where(t => !string.IsNullOrWhiteSpace(t))) }).ToList();
        }
        public List<string> Process(string cm, bool parallel)
        {
            try
            {
                var tokens = Tokenize(cm);
                var stems = ConvertAndStem(tokens, parallel);
                var filtered = FilterWords(stems);
                return filtered.SelectMany(x => x.Stems).ToList();
            }
            catch (Exception e)
            {
                _logger.Error("Preprocess Exception: " + e);
                return new List<string>();
            }
        }
        public List<StemToken> FilterWords(List<StemToken> tokens)
        {
            //detecting non-english reviews
            var errorRate = (double)tokens.Count(x => x.SpellError) / tokens.Count();
            if (errorRate >= MaxEnglishSpellErrorRate)
            {
                //we suppose its not in English 
                return new List<StemToken>();
            }

            //not informative comments
            var distincts = tokens.SelectMany(x => x.Stems).Distinct().ToList();
            if (distincts.Count() < MinInformativeDistinctWords)
                return new List<StemToken>();

            return tokens;
        }
        public List<string> Tokenize(string cm, bool preserveConsecutiveChars = false)
        {
            try
            {
                if (string.IsNullOrEmpty(cm)) return new List<string>();
                var normalized = CharacterNormalizer.Normalize(cm, preserveConsecutiveChars);
                var tokens = Tokenizer.Tokenize(normalized, preserveConsecutiveChars);
                return tokens;
            }
            catch (Exception e)
            {
                _logger.Error("Tokenize Exception: " + e);
                return new List<string>();
            }
        }
        public  List<StemToken> ConvertAndStem(List<string> tokens, bool parallel)
        {
            try
            {
                if (parallel)
                    return _stemmer.StemParallel(tokens);
                else return _stemmer.Stem(tokens);

            }
            catch (Exception e)
            {
                _logger.Error("Conver&Stem Exception: " + e);

                return new List<StemToken>();
            }
        }

        public Facade(string rootFolder)
        {
            try
            {
                if (rootFolder == null)
                {
                    throw new Exception("base address is null");
                }
                _stemmer = new Stemmer(rootFolder);
            }
            catch (Exception e)
            {
                _logger.Error("Preprocessor Init Exception: " + e);
            }
        }
        public Facade()
        {
            try
            {
                _stemmer = new Stemmer();
            }
            catch (Exception e)
            {
                _logger.Error("Preprocessor Init Exception: " + e);
            }
        }
    }
}
