﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Preprocessor
{
    public static class Helper
    {

        public static bool IsUrl(string token)
        {

            //string pattern = @"^(http|https|ftp|)\://|[a-zA-Z0-9\-\.]+\.[a-zA-Z](:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*[^\.\,\)\(\s]$";
            string pattern = @"\b(?:https?://|www\.)\S+\b";
            Regex reg = new Regex(pattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            var res = reg.IsMatch(token);
            return res;
        }


        public static List<string> FindUrls(string src)
        {
            //string pattern = @"^(http|https|ftp|)\://|[a-zA-Z0-9\-\.]+\.[a-zA-Z](:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*[^\.\,\)\(\s]$";
            string pattern = @"\b(?:https?://|www\.)\S+\b";
            Regex reg = new Regex(pattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            List<String> UrlTokens = new List<String>();
            foreach (Match m in reg.Matches(src))
            {
                UrlTokens.Add(m.Value);
            }
            return UrlTokens;
        }

        public static string RemoveUrls(string src)
        {
            //string pattern = @"^(http|https|ftp|)\://|[a-zA-Z0-9\-\.]+\.[a-zA-Z](:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*[^\.\,\)\(\s]$";
            string pattern = @"\b(?:https?://|www\.)\S+\b";
            var res = Regex.Replace(src, pattern, "");
            return res;
        }


        public static bool IsEnglish(string token)
        {
            return token.Any(IsEnglish);
        }

        public static bool IsEnglish(char c)
        {
            return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
        }


        public static bool IsPhone(string token)
        {
            string pattern = @"(0|\+98|98|0098)?([ ]|-|[()]){0,2}9[1|2|3|4]([ ]|-|[()]){0,2}(?:[0-9]([ ]|-|[()]){0,2}){8}";
            Regex reg = new Regex(pattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            var res = reg.IsMatch(token);
            return res;
        }


        public static bool IsUserName(string token)
        {
            //todo check all chars being english or symbols
            return Regex.IsMatch(token, @"@([A-Za-z0-9_\.]+)");
            //return token.StartsWith("@");
        }

        public static string RemoveUserNames(string text)
        {
            return Regex.Replace(text, @"@([A-Za-z0-9_\.]+)", " ");
        }

        public static bool IsDigit(char c)
        {
            return (c >= '0' && c <= '9');
        }


        public static bool IsDigit(string token)
        {
            return token.All(IsDigit);
        }


        public static bool IsInUrl(string src, int index)
        {
            var split = src.Substring(0, index);
            var spaceIndex = split.LastIndexOf(' ');
            if (spaceIndex < 0) spaceIndex = 0;
            var maybeurl = split.Substring(spaceIndex);

            return IsUrl(maybeurl) || IsUserName(maybeurl);
        }


        public static bool IsSpaceNeededInBetween(char c1, char c2)
        {
            if (c1 != default(char) && c1 != ' '
                        && ((IsDigit(c1) && !IsDigit(c2)) || (IsDigit(c2) && !IsDigit(c2))))
                //todo split english && persian chars
                return true;
            return false;
        }


    }
}
