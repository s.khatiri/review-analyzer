﻿using System.Collections.Generic;
using System.Linq;

namespace Preprocessor
{
    public class Tokenizer
    {
        private static char[] Seprators = { ' ', '\n', ',', ';', '\t', '/', '@', '.', ':', '!', '-', '_', '=', '?', '%', '#', '\\' };
        public static List<string> Tokenize(string srcStr, bool preserveConsecutiveChars = false)
        {
            var urls = Helper.FindUrls(srcStr);
            var urlsOmitted = Helper.RemoveUrls(srcStr);

            var tokens = srcStr.Split(Seprators).Where(x => !string.IsNullOrWhiteSpace(x) && x != "\u200c")
                //.Select(x => CharacterNormalizer.OmitConsecutiveToken(x, preserveConsecutiveChars))
                //.SelectMany(x => x)
                .ToList();
            tokens.AddRange(urls);



            //var single = ConcatSingleChars(tokens);
            //return single.Where(x => !string.IsNullOrEmpty(x)).ToList();
            return tokens;
        }

        private static List<string> ConcatSingleChars(List<string> tokens)
        {
            var singles = tokens.Select(x => x.Length == 1);


            var dest = new List<string>();
            var serialSingles = string.Empty;
            for (int i = 0; i < tokens.Count; i++)
            {
                if (tokens[i].Length == 1)
                {
                    serialSingles += tokens[i];
                }
                else
                {
                    if (!string.IsNullOrEmpty(serialSingles))
                    {
                        dest.Add(serialSingles);
                        serialSingles = string.Empty;
                    }
                    dest.Add(tokens[i]);
                }
            }
            if (!string.IsNullOrEmpty(serialSingles))
                dest.Add(serialSingles);
            return dest;
        }
    }
}
