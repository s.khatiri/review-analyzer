﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Preprocessor
{
    public  class Stemmer
    {
        private Dictionary _dictionary;
        public int ReturnedSuggestionsCount = 1;

        public Stemmer()
        {
            var base_path = Environment.GetEnvironmentVariable("DICTIONARY_FOLDER");
            if(string.IsNullOrEmpty(base_path))
                base_path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Remove(0, 6);

            _dictionary = new Dictionary(base_path + @"/en_US.aff", base_path + @"/en_US.dic");
            //_stopWords = new StopWords(base_path + @"/Resources/fa_en.stp");
        }
        public Stemmer(string rootFolder)
        {
            _dictionary = new Dictionary(rootFolder + "/en_US.aff", rootFolder + "/en_US.dic");
            //_stopWords = new StopWords(rootFolder + "/fa_en.stp");
        }
        private List<StemToken> Stem(string word, int sugCount)
        {
            if (Helper.IsUrl(word) || Helper.IsUserName(word))
            {
                return new List<StemToken>() { new StemToken(word) };
            }
            if (Helper.IsEnglish(word))
            {
                var isCorrectEnglish = StemCorrectsByDictionary(word.ToLower(), _dictionary, "en");
                if (isCorrectEnglish != null)
                {
                    return new List<StemToken>() { isCorrectEnglish };
                }
                else
                {
                    return StemSuggestedByDictionary(word, _dictionary, "en", sugCount);
                }
            }
            return new List<StemToken>() { new StemToken(word) };
        }
        private StemToken StemCorrectsByDictionary(string word, Dictionary dic, string lang)
        {
            if (dic.Spell(word))
            {
                var stem = dic.Stem(word);
                return new StemToken(word, stem, lang, false);
                //{
                //    StopWord = _stopWords.IsStopWord(word)
                //};
            }
            return null;
        }

        private List<StemToken> StemSuggestedByDictionary(string word, Dictionary dic, string lang, int suggestionsCount)
        {
            var sugs = dic.Suggest(word).Take(suggestionsCount)
                .Select(x => new StemToken(word, dic.Stem(x), lang, true)).ToList();
            return sugs;
        }

        public List<StemToken> Stem(List<string> tokens)
        {
            var stems = new List<StemToken>();
            stems = tokens.Select(x => Stem(x, ReturnedSuggestionsCount))
                .SelectMany(x => x).ToList();
            return stems;
        }
        public List<StemToken> StemParallel(List<string> tokens)
        {
            var stems = new List<StemToken>();
            stems = tokens.AsParallel().AsOrdered().Select(x => Stem(x, ReturnedSuggestionsCount))
                .SelectMany(x => x).ToList();
            return stems;
        }
    }
}
