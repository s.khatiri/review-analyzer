﻿using System.Collections.Generic;
using System.Linq;
using WeCantSpell.Hunspell;

namespace Preprocessor
{
    public class Dictionary 
    {

        private WordList _dictionary;

        public Dictionary(string affFile, string dictFile)
        {
            _dictionary = WordList.CreateFromFiles(dictFile, affFile);
        }

        public bool Spell(string word)
        {
            return _dictionary.Check(word);
        }

        public List<string> Suggest(string word)
        {
            var sugs = _dictionary.Suggest(word).ToList();
            return sugs;
        }
        public List<string> Stem(string word)
        {
            var stems = new List<string>();
            var parts = word.Split(' ').OrderByDescending(x => x.Length).ToList();
            foreach (var part in parts)
            {
                var det = _dictionary.CheckDetails(word);
                if (det.Correct && det.Root == null)
                    stems.Add(word);
                if (det.Root != null) stems.Add(det.Root);
            }
            if (stems.Contains(word))
            {
                stems = new List<string>() { word };
            }

            return stems;
        }
    }
}
