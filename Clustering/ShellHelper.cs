﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

public static class ShellHelper
{
    public static string Bash(this string cmd)
    {
        var escapedArgs = cmd.Replace("\"", "\\\"");
        var isWin = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);

        var process = new Process()
        {
            StartInfo = new ProcessStartInfo
            {
                FileName = isWin?"cmd.exe": "/bin/bash",
                Arguments =isWin? $"/c \"{escapedArgs}\"" : $"-c \"{escapedArgs}\"",
                RedirectStandardOutput = true,
                UseShellExecute = false,
                CreateNoWindow = false,
                WindowStyle=ProcessWindowStyle.Normal
            }
        };
        process.Start();
        string result = process.StandardOutput.ReadToEnd();
        process.WaitForExit();
        return result;
    }
}