﻿using Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NLog;
using CsvHelper.Configuration;
using CsvHelper;

namespace Clustering
{
    public class Mallet
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        public static string ImportCmd = "mallet import-file  --input {0} --output {1} --stoplist-file {2} --keep-sequence";
        public static string LdaCmd= "mallet train-topics --input {0} --output-doc-topics {1}  --output-topic-keys {2} --num-topics {3} --num-iterations {4} --num-top-words {5} --optimize-interval 10 --optimize-burn-in 20 ";
        public static string StopWordsAdd;
        public Mallet()
        {
            var base_path = Environment.GetEnvironmentVariable("DICTIONARY_FOLDER");
            if (string.IsNullOrEmpty(base_path))
                base_path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Remove(0, 6);
            StopWordsAdd = base_path + "/stopwords.txt";
        }

        public HashSet<Review_Taxonomy> Cluster(List<AppReview> reviews,int topicCount,int iterations,int wordsCount,double threshold)
        {
            _logger.Trace("writing mallet input file");
            var inputAdd = FileHelper.CreateTempFile("csv");
            var inputData = reviews.Select(x => new List<string>() { x.ReviewId, x.PreProcessedText }).ToList();
            FileHelper.WriteCsv(inputAdd, inputData,"\t",false);

            _logger.Trace("importing data to mallet");
            var dataAdd = FileHelper.CreateTempFile("bin");
            var import = string.Format(ImportCmd, inputAdd,dataAdd,StopWordsAdd);
            var res1=import.Bash();

            _logger.Trace("starting lda");
            var topicsAdd = FileHelper.CreateTempFile("csv");
            var docsAdd = FileHelper.CreateTempFile("csv");

            var lda = string.Format(LdaCmd, dataAdd, docsAdd,topicsAdd,topicCount,iterations,wordsCount);
            var res2 = lda.Bash();

            return ReadLdaResult(reviews, topicsAdd, docsAdd,threshold);
            

        }

        private HashSet<Review_Taxonomy> ReadLdaResult(List<AppReview> reviews,string topicsAdd,string docsAdd,double threshold)
        {
            _logger.Trace("reading lda topics");
            var topics_raw = FileHelper.CsvToList(topicsAdd, "\t");

            var taxonomies = topics_raw.Select(x => new Taxonomy()
            {
                TopicId = int.Parse(x[0].Trim()),
                Keywords = x[2].Split(' ').Where(y => !string.IsNullOrWhiteSpace(y)).ToList(),
                Name = string.Join("_", x[2].Split(' ').Take(3))

            }).ToList();


            _logger.Trace("reading lda documents");
            var review_taxonomies = new HashSet<Review_Taxonomy>();
            var other_taxonomy = new Taxonomy()
            {
                Name = "Other Reviews"
            };


            using (var reader = File.OpenText(docsAdd))
            {
                var conf = new Configuration()
                {
                    HasHeaderRecord = false,
                    AllowComments = false,
                    IgnoreBlankLines = true,
                    Delimiter = "\t",
                    BadDataFound = null

                };
                var parser = new CsvParser(reader, conf);
                var r = 0;
                while (true)
                {
                    
                    var docDistributions = parser.Read();

                    if (docDistributions == null)
                    {
                        break;
                    }

                    var isClassified = false;
                    for (var t = 0; t < taxonomies.Count; t++)
                    {
                        var rt = new Review_Taxonomy()
                        {
                            Review = reviews[r],
                            TaxonomyNode = taxonomies[t],
                            Score = float.Parse(docDistributions[t + 2].Trim())
                        };
                        if (rt.Score >= threshold)
                        {
                            review_taxonomies.Add(rt);
                            isClassified = true;
                        }
                    }
                    if (!isClassified)
                    {
                        var rt = (new Review_Taxonomy
                        {
                            Review = reviews[r],
                            TaxonomyNode = other_taxonomy,
                            Score = 0
                        });
                        review_taxonomies.Add(rt);
                    }
                    r++;
                }

            }

            return review_taxonomies;

        }

    }
}
