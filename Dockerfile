FROM mcr.microsoft.com/dotnet/core/aspnet:2.1-stretch-slim AS base

########install java 8
RUN mkdir -p /usr/share/man/man1 && \
    apt-get update -y && \
    apt-get install -y openjdk-8-jdk

RUN apt-get install unzip -y && \
    apt-get autoremove -y
    
#######intstall mellet
ADD http://mallet.cs.umass.edu/dist/mallet-2.0.8.tar.gz /
RUN tar -zxf /mallet-2.0.8.tar.gz
ENV PATH="/mallet-2.0.8/bin:${PATH}"



WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:2.1-stretch AS build
WORKDIR /src
COPY ["Website/AppReviewAnalyzer.csproj", "Website/"]
COPY ["Preprocessor/Preprocessor.csproj", "Preprocessor/"]
COPY ["Data/Data.csproj", "Data/"]
COPY ["Clustering/Clustering.csproj", "Clustering/"]
RUN dotnet restore "Website/AppReviewAnalyzer.csproj"
COPY . .
WORKDIR "/src/Website"
RUN dotnet build "AppReviewAnalyzer.csproj" -c Release -o /app


FROM build AS publish
RUN dotnet publish "AppReviewAnalyzer.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "AppReviewAnalyzer.dll"]