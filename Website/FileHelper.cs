﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Data;
using CsvHelper;
using CsvHelper.Configuration;
//using ExcelDataReader;

namespace Data
{
    public static class FileHelper
    {
        public static List<InferenceData> ReadReviewsFromCsv_TestTemplate(string fileAdd, string fileName)
        {
            var records = CsvToList(fileAdd, "\t");
            var inferenceDatas = records.Select(x => new InferenceData()
            {
                Text = x.ElementAt(5) + " " + x.ElementAt(6)
                //ActualLabel = x.ElementAt(2)
            });
            return inferenceDatas.ToList();
        }
        public static List<AppReview> ReadReviewsFromCsv_DatasetTemplate(string fileAdd,string appName)
        {
            var records = CsvToList(fileAdd,"\t");
            var reviews = records.Select(x => new AppReview(x.ElementAt(1), appName, x.ElementAtOrDefault(0), x.ElementAtOrDefault(5), x.ElementAtOrDefault(6),null));
            return reviews.ToList();
        }
        public static List<AppReview> ReadReviewsFromCsv(string fileAdd)
        {
            var records = CsvToList(fileAdd);
            var reviews=records.Select(x => new AppReview(x.ElementAtOrDefault(0), x.ElementAtOrDefault(1), x.ElementAtOrDefault(2), x.ElementAtOrDefault(3), x.ElementAtOrDefault(4),x.ElementAtOrDefault(5)));
            return reviews.ToList();
        }
        public static void WriteReviewsToCsv(string fileAdd, List<AppReview> reviews)
        {
            
            var records = reviews.Select(x => new List<string> {x.ReviewId,x.App,x.Rate,x.Title,x.Review,x.PreProcessedText }).ToList();
            WriteCsv(fileAdd, records);
        }
      
        public static List<List<string>> CsvToList(string csvAddress,string delimiter=",")
        {
            var records = new List<List<string>>();
            using (var reader = File.OpenText(csvAddress))
            {
                var conf = new Configuration()
                {
                    HasHeaderRecord = false,
                    AllowComments = false,
                    IgnoreBlankLines = true,
                    Delimiter=delimiter,
                    BadDataFound=null

                };
                var parser = new CsvParser(reader, conf);
                while (true)
                {
                    var row = parser.Read();

                    if (row == null)
                    {
                        break;
                    }
                    records.Add(row.ToList());
                }

            }
            return records;

        }


        public static void WriteCsv(string csvAddress, List<List<string>> data,string delimiter=",",bool qouteAll=true)
        {
            using (var writer = File.CreateText(csvAddress))
            {
                var conf = new Configuration()
                {
                    HasHeaderRecord = false,
                    AllowComments = false,
                    IgnoreBlankLines = true,
                    Delimiter = delimiter,
                    ShouldQuote=((x,y)=> qouteAll)
                };
                var csvWriter = new CsvWriter(writer, conf);
                data.ForEach(x =>
                {
                    x.ForEach(y =>
                    {
                        csvWriter.WriteField(y);
                    });
                    csvWriter.NextRecord();
                });
            }
        }
        public static void WriteCsv(string csvAddress, List<List<string>> data, List<int> cols)
        {
            var selected = FilterColumns(data, cols);

            WriteCsv(csvAddress, selected);
        }
        public static List<List<string>> FilterColumns(List<List<string>> data, List<int> cols)
        {
            if (cols == null) return data;
            var selected = data.Select(x =>
                cols.Select(c => x.ElementAtOrDefault(c)).ToList())
                .ToList();
            return selected;
        }

        public static string CreateTempFile(string extension)
        {
            var tempPath = Path.GetTempFileName();
            var filePath = Path.ChangeExtension(tempPath, extension);
            return filePath;
        }
    }
}
