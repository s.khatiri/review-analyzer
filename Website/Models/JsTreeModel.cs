﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data;
using MongoDB.Bson;

namespace AppReviewAnalyzer.Models
{

    public class Data
    {
        public long reviewCount { get; set; }
        public string keywords { get; set; }
    }

    public class JsTreeModel
    {
        public string id { get; set; }
        public string parent { get; set; }
        public string text { get; set; }
        public string icon { get; set; }
        public string state { get; set; }
        public bool opened { get; set; }
        public bool disabled { get; set; }
        public bool selected { get; set; }
        public string li_attr { get; set; }
        public string a_attr { get; set; }
        public Data data { get; set; }

        public JsTreeModel()
        {
            data = new Data();
        }

        public static List<JsTreeModel> GenerateTree(Taxonomy taxonomy)
        {
            var res = new List<JsTreeModel>();

            res.Add(new JsTreeModel()
            {
                id = taxonomy.Id.ToString(),
                text = taxonomy.Name,
                parent = "#"
            }); ; ;

            Stack<Taxonomy> stack = new Stack<Taxonomy>();

            stack.Push(taxonomy);
            Taxonomy current;

            while (stack.Count > 0)
            {
                current = stack.Pop();
                foreach (var ch in current.Children)
                {
                    res.Add(new JsTreeModel()
                    {
                        id = ch.Id.ToString(), 
                        text = ch.Name,
                        parent = current.Id.ToString(),
                        data = new Data { reviewCount = ch.ReviewCount(), keywords = String.Join(", ", ch.Keywords) }
                    });
                    stack.Push(ch);
                }
            }

            return res;
        }

        public static List<JsTreeModel> GenerateTree(TrainedTaxonomy taxonomy, ObjectId testBulkId)
        {
            var res = new List<JsTreeModel>();

            res.Add(new JsTreeModel()
            {
                id = taxonomy.Id.ToString(),
                text = taxonomy.Name,
                parent = "#"
            }); ; ;

            Stack<TrainedTaxonomy> stack = new Stack<TrainedTaxonomy>();

            stack.Push(taxonomy);
            TrainedTaxonomy current;

            while (stack.Count > 0)
            {
                current = stack.Pop();
                foreach (var ch in current.Children)
                {
                    res.Add(new JsTreeModel()
                    {
                        id = ch.Id.ToString(),
                        text = ch.Name,
                        parent = current.Id.ToString(),
                        data = new Data { reviewCount = ch.ReviewCount(testBulkId) }
                    });
                    stack.Push(ch);
                }
            }

            return res;
        }
    }
}
