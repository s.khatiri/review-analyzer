﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppReviewAnalyzer.Models
{
    public class MergeViewModel
    {
        public string RootId { get; set; }
        public List<string> TaxonomyIds { get; set; }
        public List<string> TaxonomyNames { get; set; }
        public bool MergingLeaves { get; set; }
        public string NewName { get; set; }
        public string ProcessName { get; set; }
    }
}
