﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppReviewAnalyzer.Models
{
    public class TrainViewModel
    {
        public string ProcessName { get; set; }
        public double Threshold { get; set; }
        public string RootId { get; set; }
    }
}
