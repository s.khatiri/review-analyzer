﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppReviewAnalyzer.Models
{
    public class ClusterMoreModel
    {
        public string TaxonomyId { get; set; }
        public string RootId { get; set; }
        public string ProcessName { get; set; }
        public string TaxonomyName { get; set; }
        public int NumberOfTopics { get; set; }
        public int NumberOfIterations { get; set; }
        public int NumberOfWords { get; set; }
        public double TopicThreshold { get; set; }

    }
}
