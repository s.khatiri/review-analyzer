﻿//using Data;
//using Newtonsoft.Json;
//using Newtonsoft.Json.Serialization;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;

//namespace AppReviewAnalyzer.Models
//{
//    public class VennDataModel
//    {
//        public List<Set> Model=new List<Set>();

//        public class Set
//        {
//            public List<string> Sets;
//            public int Size;
//        }

//        public VennDataModel(List<Topic> topics)
//        {
//            var sets=topics.Select(x=>new KeyValuePair<string,HashSet<string>>(x.Name,x.Reviews.Select(y=>y.Id.ToString()).ToHashSet())).ToList();
//            for (var i= 0; i < sets.Count(); i++)
//            {
//                Model.Add(new Set()
//                {
//                    Sets = new List<string> { sets[i].Key },
//                    Size = sets[i].Value.Count()
//                });
//                for (var j = i+1; j < sets.Count(); j++)
//                {
//                    Model.Add(new Set()
//                    {
//                        Sets = new List<string> { sets[i].Key,sets[j].Key },
//                        Size = sets[i].Value.Intersect(sets[j].Value).Count()
//                    });
//                }
//            }
//        }
//        public string ToJson()
//        {
//            return JsonHelper.Convert(Model);
//        }
//    }
//}
