﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Data;
using AppReviewAnalyzer.Models;
using Newtonsoft.Json;
using Clustering;
using MongoDB.Bson;
using NLog;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AppReviewAnalyzer.Controllers
{
    public class TaxonomyController : Controller
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        // GET: /<controller>/
        public IActionResult List()
        {
            var clusters = Taxonomy.GetClusters();

            return View(clusters);
        }

        public IActionResult CreateCluster()
        {
            var apps = AppReview.GetApps();
            //var topics = Topic.Get();
            var model = new CreateClustersModel() { Apps = apps };
            return View(model);
        }

        public IActionResult Cluster(string name, List<string> apps, int topics = 50, int iterations = 1000, int words = 20, double threshold = 0.2)
        {

            _logger.Info("creating new taxonomy");
            var reviews = new List<AppReview>();

            _logger.Info("loading AppReviews");
            // if the user has selected a customized app set (and not all the apps)
            if (apps != null && apps.Count > 0 && !apps.Contains("all"))
            {
                reviews = AppReview.Get(apps);
            }
            // if the user has chosen to cluster everythung
            else
            {
                reviews = AppReview.Get();
            }
            _logger.Info("{0} Reviews were loaded", reviews.Count);

            
            var mallet = new Mallet();
            var review_taxonomies = mallet.Cluster(reviews, topics, iterations, words,threshold).ToList();

            var taxonomies= review_taxonomies.Select(x => x.TaxonomyNode).Distinct().ToList();

            //var chosen_rts = review_taxonomies.Where(x => x.Score > threshold);
            //_logger.Info("{0} valid topic distributions found", chosen_rts.Count());
            //var reviews_with_taxonomies = chosen_rts.Select(x => x.Review).Distinct();
            //_logger.Info("{0} relevent documents to topics found", reviews_with_taxonomies.Count());
            //var reviews_without_taxonomies = reviews.Except(reviews_with_taxonomies);
            //_logger.Info("{0} irrelevent documents to any topics found", reviews_without_taxonomies.Count());

            //var taxonomies = chosen_rts.Select(x => x.TaxonomyNode).Distinct().ToList();

            //var other_taxonomy = new Taxonomy()
            //{
            //    Name = "Other Reviews"
            //};
            //taxonomies.Add(other_taxonomy);

            //var other_rts = new List<Review_Taxonomy>();
            //reviews_without_taxonomies.ToList().ForEach(x => {
            //    other_rts.Add(new Review_Taxonomy()
            //    {
            //        TaxonomyNode = other_taxonomy, 
            //        Score = 0, 
            //        Review = x
            //    });
            //});

            taxonomies.ForEach(x => {
                x.ProcessName = name;
                x.Children = new List<Taxonomy>();
                x.Reviews = new List<AppReview>();
            });


            var rootTaxonomy = new Taxonomy()
            {
                ProcessName = name,
                Name = "Root_" + name,
                Children = taxonomies
            };

            _logger.Info("saving taxonomy");
            rootTaxonomy.Save();


            review_taxonomies.ForEach(x => x.TaxonomyRoot = rootTaxonomy);
            //chosen_rts.ToList().ForEach(x => x.TaxonomyRoot = rootTaxonomy);
            //other_rts.ForEach(x => x.TaxonomyRoot = rootTaxonomy);


            _logger.Info("saving topic distributions");
            Review_Taxonomy.InsertAll(review_taxonomies);
            //Review_Taxonomy.InsertAll(chosen_rts.ToList());
            //if (other_rts.Count() > 0)
            //    Review_Taxonomy.InsertAll(other_rts);

            return RedirectToAction("List", "Taxonomy", new { processName = name });
        }


        public IActionResult Tree(string processName)
        {
            var taxonomy = Taxonomy.GetByCluster(processName);

            //if (Request.Headers["X-Requested-With"] == "XMLHttpRequest")
            //{
            //    return PartialView(taxonomy);
            //}
            return View(taxonomy);
        }

        public IActionResult Nodes(string processName)
        {
            //var nodes = new List<JsTreeModel>();
            var taxonomy = Taxonomy.GetByCluster(processName);
            var nodes = JsTreeModel.GenerateTree(taxonomy);
            return Json(nodes);
        }

        public IActionResult SeeReviews(string taxonomyId, string rootId, int count = 100)
        {

            var taxonomy = Taxonomy.GetGenericNode(rootId, taxonomyId);
            taxonomy.AnnotateReviews(count);
            ViewData["rootId"] = rootId;
            ViewData["taxonomyId"] = taxonomyId;
            ViewData["count"] = count;
            return View(taxonomy.Reviews);
        }

        public IActionResult ClusterMore(string taxonomyId, string rootId, string processName)
        {
            var toid = ObjectId.Parse(taxonomyId);
            var roid = ObjectId.Parse(rootId);
            var root = Taxonomy.GetById(roid);

            var taxonomy = root.SearchRecursive(toid);
            var model = new ClusterMoreModel
            {
                TaxonomyId = taxonomyId,
                RootId = rootId,
                TaxonomyName = taxonomy.Name,
                ProcessName = processName
            };

            return View(model);
        }

        [HttpPost]
        public IActionResult ClusterMore(ClusterMoreModel clusterMoreModel)
        {
            var root = Taxonomy.GetById(ObjectId.Parse(clusterMoreModel.RootId));
            var taxonomy = root.SearchRecursive(ObjectId.Parse(clusterMoreModel.TaxonomyId));
            taxonomy.AnnotateReviews(int.MaxValue);
            Review_Taxonomy.DeleteByTaxonomy(taxonomy.Id);


            var reviews = taxonomy.Reviews;
           
            var mallet = new Mallet();
            var review_taxonomies = mallet.Cluster(reviews, clusterMoreModel.NumberOfTopics, clusterMoreModel.NumberOfIterations, clusterMoreModel.NumberOfWords,clusterMoreModel.TopicThreshold).ToList();

            //var chosen_rts = review_taxonomies.Where(x => x.Score > clusterMoreModel.TopicThreshold).ToList();

            //var reviews_with_taxonomies = chosen_rts.Select(x => x.Review).Distinct().ToList();
            //var reviews_without_taxonomies = reviews.Except(reviews_with_taxonomies).ToList();


            var taxonomies = review_taxonomies.Select(x => x.TaxonomyNode).Distinct().ToList();
            //var taxonomies = chosen_rts.Select(x => x.TaxonomyNode).Distinct().ToList();

            //var other_taxonomy = new Taxonomy()
            //{
            //    Name = "Other Reviews"
            //};
            //taxonomies.Add(other_taxonomy);

            taxonomies.ForEach(x => {
                x.ProcessName = clusterMoreModel.ProcessName;
                x.Children = new List<Taxonomy>();
                x.Reviews = new List<AppReview>();
            });

            //var other_rts = new List<Review_Taxonomy>();
            //reviews_without_taxonomies.ForEach(x => {
            //    other_rts.Add(new Review_Taxonomy()
            //    {
            //        TaxonomyNode = other_taxonomy,
            //        Score = 0,
            //        Review = x
            //    });
            //});

            taxonomy.Children = taxonomies;
            root.Save();

            review_taxonomies.ForEach(x => x.TaxonomyRoot = root);
            //chosen_rts.ForEach(x => x.TaxonomyRoot = root);
            //other_rts.ForEach(x => x.TaxonomyRoot = root);

            Review_Taxonomy.InsertAll(review_taxonomies);
            //Review_Taxonomy.InsertAll(chosen_rts);
            //if (other_rts.Count() > 0)
            //    Review_Taxonomy.InsertAll(other_rts);

            return RedirectToAction("Tree", "Taxonomy", new { processName = clusterMoreModel.ProcessName });    

        }

        public IActionResult Rename(string taxonomyId, string rootId, string newName)
        {
            var roid = ObjectId.Parse(rootId);
            var toid = ObjectId.Parse(taxonomyId);
            var root = Taxonomy.GetById(roid);
            var taxonomy = root.SearchRecursive(toid);

            taxonomy.UpdateName(root, newName);
            
            return Ok();
        }

        public IActionResult Create(string parentId, string rootId, string nodeName, string newId)
        {
            var roid = ObjectId.Parse(rootId);
            var poid = ObjectId.Parse(parentId);
            var root = Taxonomy.GetById(roid);
            var parent = root.SearchRecursive(poid);
            var taxonomy = new Taxonomy()
            {
                Id = ObjectId.Parse(newId),
                ProcessName = root.ProcessName,
                Name = nodeName
            };
            parent.Children.Add(taxonomy);
            root.Save();

            return Ok();
        }

        public IActionResult DeleteSubtree(string rootId, string taxonomyId)
        {
            var roid = ObjectId.Parse(rootId);
            var toid = ObjectId.Parse(taxonomyId);
            var root = Taxonomy.GetById(roid);
            root.DeleteSubtree(toid);
            return Ok();
        }

        public IActionResult Move(string taxonomyId, string rootId, string newParentId)
        {
            var roid = ObjectId.Parse(rootId);
            var poid = ObjectId.Parse(newParentId);
            var toid = ObjectId.Parse(taxonomyId);
            var root = Taxonomy.GetById(roid);
            var newParent = root.SearchRecursive(poid);
            var taxonomy = root.SearchRecursive(toid);
            var oldParent = taxonomy.FindParent(root);
            oldParent.Children.Remove(taxonomy);
            newParent.Children.Add(taxonomy);
            root.Save();
            return Ok();
        }

        public IActionResult Merge(List<string> taxonomyIds, string rootId, bool mergingLeaves)
        {
            var roid = ObjectId.Parse(rootId);
            var root = Taxonomy.GetById(roid);

            List<string> taxonomyNames = new List<string>();
            taxonomyIds.ForEach(x =>
            {
                taxonomyNames.Add(root.SearchRecursive(ObjectId.Parse(x)).Name);
            });

            var model = new MergeViewModel()
            {
                TaxonomyNames = taxonomyNames,
                TaxonomyIds = taxonomyIds,
                ProcessName = root.ProcessName,
                RootId = rootId,
                MergingLeaves = mergingLeaves
            };
            return View(model);
            
        }

        [HttpPost]
        public IActionResult Merge(MergeViewModel mergeViewModel)
        {
            var roid = ObjectId.Parse(mergeViewModel.RootId);
            var root = Taxonomy.GetById(roid);
            var taxonomies = new List<Taxonomy>();
            //var reviewIds = new List<string>();
            var oldRts = new List<Review_Taxonomy>();
            


            mergeViewModel.TaxonomyIds.ForEach(x => {
                taxonomies.Add(root.SearchRecursive(ObjectId.Parse(x)));
                if (mergeViewModel.MergingLeaves)
                {
                    var currentRts = Review_Taxonomy.GetRtsByTaxonomy(ObjectId.Parse(x));
                    currentRts.ForEach(y => {
                        //reviewIds.Add(y.ToString());
                        oldRts.Add(y);
                    });
                    Review_Taxonomy.DeleteByTaxonomy(ObjectId.Parse(x));
                }

            });
            //reviewIds = reviewIds.Distinct().ToList();

            var parent = taxonomies[0].FindParent(root);

            taxonomies.ForEach(x => {
                var correspondingChild = parent.Children.Find(y => y.Id == x.Id);
                parent.Children.Remove(correspondingChild);
            });

            var newTaxonomy = new Taxonomy()
            {
                Name = mergeViewModel.NewName, 
                ProcessName = mergeViewModel.ProcessName, 
                
            };
            parent.Children.Add(newTaxonomy);
            root.Save(); // so that newTaxonomy gets an id

            if (mergeViewModel.MergingLeaves)
            {
                var rts = new List<Review_Taxonomy>();
                oldRts.ForEach(x => {
                    rts.Add(new Review_Taxonomy()
                    {
                        ReviewId = x.ReviewId, 
                        TaxonomyNode = newTaxonomy, 
                        TaxonomyRoot = root, 
                        Score = x.Score
                    });
                });
                Review_Taxonomy.InsertAll(rts);

                taxonomies.ForEach(x => {
                    x.Keywords.ForEach(y =>
                    {
                        if (!newTaxonomy.Keywords.Contains(y))
                        {
                            newTaxonomy.Keywords.Add(y);
                        }
                    });
                });
            }
            else
            {
                newTaxonomy.Children = taxonomies;
            }

            root.Save();

            return RedirectToAction("Tree", "Taxonomy", new { processName = mergeViewModel.ProcessName });
        }

        public IActionResult Train(string rootId)
        {
            var roid = ObjectId.Parse(rootId);
            var root = Taxonomy.GetById(roid);
            var model = new TrainViewModel()
            {
                RootId = rootId, 
                ProcessName = root.ProcessName,
                Threshold=0.5
                
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult Train(TrainViewModel tvm)
        {
            var roid = ObjectId.Parse(tvm.RootId);
            var root = Taxonomy.GetById(roid);
            root.Train(tvm.ProcessName,tvm.Threshold);
            return RedirectToAction("List", "TrainedTaxonomy");
        }


        //public IActionResult Train(string rootId)
        //{
        //    var roid = ObjectId.Parse(rootId);
        //    var root = Taxonomy.GetById(roid);
        //    root.Train();
        //    return null;
        //}


        public IActionResult Venn(string processName)
        {
            //var topics = Topic.Get(id);
            //topics.ForEach(x => x.Reviews = Review_Topic.GetByTopic(x.Id, int.MaxValue,false));
            //var model = new VennDataModel(topics);
            //return View(model);
            return null;

        }
        public IActionResult FoamTree(string processName, int count = 100)
        {
            //var topics = Topic.Get(id);
            //var taxonomies = Taxonomy.GetByCluster(id);
            //taxonomies.ForEach(x => x.Reviews = Review_Topic.GetByTopic(x.Id,count));
            //var model = new FoamTreeDataObject(topics);
            //return View(model);
            return null;
        }
    }
}
