﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Preprocessor;
using NLog;

namespace AppReviewAnalyzer.Controllers
{
    public class AppReviewController : Controller
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        // a form to upload csv file
        public IActionResult Import()
        {
            return View();
        }

        // not implemented yet
        public IActionResult List()
        {
            return View();
        }

        // not implemented yet
        public IActionResult Report()
        {
            return View();
        }

        // file is posted to this action
        [HttpPost]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> UploadFile(List<IFormFile> files)
        {
            _logger.Info("importing AppReviews");
            if (files == null || !files.Any()) return BadRequest("no files are selected");
            var savedFiles = new Dictionary<string, string>();
            _logger.Info("{0} files are being uploaded", files.Count);
            foreach (var file in files)
            {
                _logger.Info("uploading {0}", file.FileName);
                if (file == null || file.Length == 0)
                    continue;

                var path = FileHelper.CreateTempFile(Path.GetExtension(file.FileName));

                using (var stream = new FileStream(path, FileMode.Create))
                {
                    file.CopyTo(stream);
                }
                savedFiles.Add(Path.GetFileNameWithoutExtension(file.FileName), path);
            }
            _logger.Info("finished uploading all files");


            Task.Run(() =>
            {
                var preprocessor = new Facade();
                var logger = LogManager.GetCurrentClassLogger();
                logger.Info("starting preprocessing files");
                foreach (var file in savedFiles)
                {
                    var records = FileHelper.ReadReviewsFromCsv_DatasetTemplate(file.Value, file.Key);
                    logger.Info("preprocessing {0} reviews in {1}", records.Count, file.Key);
                    records.AsParallel().ForAll(x =>
                    {
                        var prep = preprocessor.Process(x.Title + " " + x.Review, false);
                        x.PreProcessedText = string.Join(" ", prep);
                    });
                    AppReview.InsertAll(records);
                }

                logger.Info("finished importing all files");
            });

            return RedirectToAction("import");
        }

        //public async Task<IActionResult> Download(string filename)
        //{
        //    if (filename == null)
        //        return Content("filename not present");

        //    var path = Path.Combine(
        //                   Directory.GetCurrentDirectory(),
        //                   "wwwroot", filename);

        //    var memory = new MemoryStream();
        //    using (var stream = new FileStream(path, FileMode.Open))
        //    {
        //        await stream.CopyToAsync(memory);
        //    }
        //    memory.Position = 0;
        //    return File(memory, "text/csv", Path.GetFileName(path));
        //}
    }
}