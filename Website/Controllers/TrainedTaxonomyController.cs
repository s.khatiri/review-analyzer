﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Data;
using System.Threading;
using MongoDB.Bson;
using Microsoft.AspNetCore.Http;
using AppReviewAnalyzer.Models;

using System.IO;
using Preprocessor;
using CsvHelper;

namespace AppReviewAnalyzer.Controllers
{
    public class TrainedTaxonomyController : Controller
    {
        public IActionResult List()
        {
            // first update all classifier statuses
            var classifiers = Classifier.GetAll();
            classifiers.ForEach(x => {
                try
                {
                    x.Status = NluUnitFacade.BuildNluModelStatus(x.CustomCode)["training_status"].ToString();
                }
                catch (RestCallException)
                {
                    x.Status = "NOT_FOUND";
                }
                x.Save();
            });

            var models = TrainedTaxonomy.GetAll();
            //models.ForEach(tt => {
            //    var classifiers = tt.GetClassifiers();
            //    classifiers.ForEach(c => {
            //        c.Status = NluUnitFacade.BuildNluModelStatus(c.CustomCode)["training_status"].ToString();
            //    });
            //});
            return View(models);
        }

        public IActionResult Train(string rootId)
        {
            var roid = ObjectId.Parse(rootId);
            var root = TrainedTaxonomy.GetById(roid);
            var classifiers = root.GetClassifiers();
            classifiers.ForEach(x => {
                NluUnitFacade.TrainClassifier(x);
                Thread.Sleep(1000);
                while (x.TrainingInProgress())
                    Thread.Sleep(3000);
            } );
            return RedirectToAction("List", "TrainedTaxonomy");
        }


        public IActionResult ListClassifiers(string rootId)
        {
            var roid = ObjectId.Parse(rootId);
            var root = TrainedTaxonomy.GetById(roid);
            var model = root.GetClassifiers();
            return View(model);
        }


        public IActionResult DownloadClassifierData(string classifierId)
        {
            var coid = ObjectId.Parse(classifierId);
            var classifier = Classifier.GetById(coid);
            var data = classifier.LoadTrainingData();

            var stream = new MemoryStream();
            var writeFile = new StreamWriter(stream);
            var csv = new CsvWriter(writeFile);

            for (var i = 0; i < data.Labels.Count(); i++)
            {
                csv.WriteField(data.Texts[i]);
                csv.WriteField(data.Labels[i]);
                csv.NextRecord();
            }
            csv.Flush();
            writeFile.Flush();
            stream.Flush();

            stream.Position = 0; //reset stream
            return File(stream, "application/octet-stream", "Classifier " + classifierId + ".csv");
        }

        public IActionResult TestBulk(string id)
        {
            var roid = ObjectId.Parse(id);
            var root = TrainedTaxonomy.GetById(roid);
            return View(root);
        }

        [HttpPost]
        public IActionResult TestBulk(string id, string processName, double threshold, List<IFormFile> files)
        {
            if (files == null || !files.Any()) return BadRequest("no files are selected");

            var savedFiles = new Dictionary<string, string>();
            var roid = ObjectId.Parse(id);
            var root = TrainedTaxonomy.GetById(roid);
            var testBulk = new TestBulk()
            {
                ProcessName = processName,
                TrainedTaxonomyRootId = roid
            };
            testBulk.Save();

            foreach (var file in files)
            {
                if (file == null || file.Length == 0)
                    continue;

                var path = FileHelper.CreateTempFile(Path.GetExtension(file.FileName));

                using (var stream = new FileStream(path, FileMode.Create))
                {
                    file.CopyTo(stream);
                }
                savedFiles.Add(Path.GetFileNameWithoutExtension(file.FileName), path);
            }

            var preprocessor = new Facade();
            List<InferenceData> inferenceDatas = new List<InferenceData>();
            foreach (var file in savedFiles)
            {
                var records = FileHelper.ReadReviewsFromCsv_TestTemplate(file.Value, file.Key);
                records.AsParallel().ForAll(x =>
                {
                    var prep = preprocessor.Process(x.Text, false);
                    x.PreProcessedText = string.Join(" ", prep);
                    x.RootId = root.Id;
                    x.TestBulkId = testBulk.Id; 

                });
                InferenceData.InsertAll(records);
                inferenceDatas.AddRange(records);
            }

            inferenceDatas.ForEach(x => {
                root.Infer(x, threshold);
                x.Save();
            });

            
            return RedirectToAction("ListTestBulks", "TrainedTaxonomy");
        }



        public IActionResult ListTestBulks()
        {
            var model = Data.TestBulk.GetAll();
            return View(model);
        }

        public IActionResult TestSingle(string id, string text)
        {
            ViewData["InputValue"] = text;

            var roid = ObjectId.Parse(id);
            var root = TrainedTaxonomy.GetById(roid);
            if (!string.IsNullOrEmpty(text))
            {
                Preprocessor.Facade prep = new Preprocessor.Facade();
                var prepText = string.Join(" ", prep.Process(text, false));
                InferenceData result = root.InferSingle(text, prepText);
                return View(result);
            }
            return View(new InferenceData { RootId = roid });
        }

        public IActionResult TestBulkTree(string trainedTaxonomyRootId, string testBulkId)
        {
            var roid = ObjectId.Parse(trainedTaxonomyRootId);
            var root = TrainedTaxonomy.GetById(roid);
            ViewData["testBulkId"] = testBulkId;
            return View(root);
        }

        public IActionResult Nodes(string trainedTaxonomyRootId, string testBulkId)
        {
            var roid = ObjectId.Parse(trainedTaxonomyRootId);
            var tboid = ObjectId.Parse(testBulkId);
            var root = TrainedTaxonomy.GetById(roid);
            var nodes = JsTreeModel.GenerateTree(root, tboid);
            return Json(nodes);
        }

        public IActionResult SeeReviews(string trainedTaxonomyId, string testBulkId, int count = 100)
        {
            var toid = ObjectId.Parse(trainedTaxonomyId);
            var tboid = ObjectId.Parse(testBulkId);
            var model = InferenceData.GetByTrainedTaxonomy(toid, tboid, count);
            ViewData["trainedTaxonomyId"] = trainedTaxonomyId;
            ViewData["count"] = count;
            return View(model);
        } 

    }
}