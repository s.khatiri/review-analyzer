﻿
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data
{
    public enum TrainedTaxonomyStatus
    {
        WaitingToStartTraining, TrainingInProgress, TrainingCompleted, TrainingError
    } 

    public class TrainedTaxonomy
    {
        [BsonId]
        public ObjectId Id { get; set; }
        public ObjectId TaxonomyRootId { get; set; }
        public List<TrainedTaxonomy> Children { get; set; }
        public string Name { get; set; }
        public ObjectId ClassifierId { get; set; }
        public string ProcessName { get; set; }
        [BsonIgnore]
        public TrainedTaxonomyStatus Status { get; set; }

        public bool HasChildren
        {
            get
            {
                if (this.Children == null || !this.Children.Any())
                    return false;
                return true;
            }
        }


        public TrainedTaxonomy()
        {
            Children = new List<TrainedTaxonomy>();
            ClassifierId = default;
            Status = TrainedTaxonomyStatus.WaitingToStartTraining;
        }

        public static IMongoCollection<TrainedTaxonomy> Collection
        {
            get => MongoDb.Database.GetCollection<TrainedTaxonomy>("TrainedTaxonomies");
        }

        public static List<TrainedTaxonomy> GetAll()
        {
            var res = Collection.Find(x => true);
            if (res.CountDocuments() <= 0)
                return new List<TrainedTaxonomy>();
            var resList = res.ToList();
            resList.ForEach(x => {
                x.Status = x.GetStatus();
            });
            return resList;
        }

        // called from root
        public TrainedTaxonomyStatus GetStatus()
        {
          
            var finished = true;
            var classifiers = this.GetClassifiers();
            List<string> clasifierStata = new List<String>();
            classifiers.ForEach(x => {
                clasifierStata.Add(x.Status);
            });
            
            
            if (clasifierStata.Contains("NOT_FOUND"))
            {
                return TrainedTaxonomyStatus.WaitingToStartTraining;
            }
            else if (clasifierStata.Contains("ERROR_TRAINING"))
            {
                return TrainedTaxonomyStatus.TrainingError;
            }
            else if (clasifierStata.Contains("TRAINING"))
            {
                return TrainedTaxonomyStatus.TrainingInProgress;
            }
            else 
            {
                return TrainedTaxonomyStatus.TrainingCompleted;
            }
            
            
        }

        public void GenerateChildrenIdRecursive()
        {
            if (this.HasChildren)
            {
                foreach (var child in this.Children)
                {
                    if (child.Id == default)
                    {
                        child.Id = ObjectId.GenerateNewId();
                    }
                    child.GenerateChildrenIdRecursive();

                }
            }
        }

        public static TrainedTaxonomy GetById(ObjectId rootId)
        {
            var res = Collection.Find(x => x.Id == rootId);
            if (res.CountDocuments() <= 0)
            {
                return null;
            }
            var t = res.FirstOrDefault();
            
            return t;
        }

        // THIS FUNCTION IS ONLY CALLED FROM ROOT OBJECT
        public void Save()
        {
            var existing = GetById(this.Id);
            this.GenerateChildrenIdRecursive();
            if (existing == null)
            {
                Collection.InsertOne(this);
            }
            else
            {
                var filter = Builders<TrainedTaxonomy>.Filter.Eq(doc => doc.Id, this.Id);
                Collection.ReplaceOne(filter, this);
            }
        }

        // called from root
        public List<ObjectId> GetClassifierIds()
        {
            List<ObjectId> classifierIds = new List<ObjectId>();
            Stack<TrainedTaxonomy> stack = new Stack<TrainedTaxonomy>();
            stack.Push(this);
            TrainedTaxonomy current;

            while (stack.Count() > 0)
            {
                current = stack.Pop();
                if (current.ClassifierId != default)
                {
                    classifierIds.Add(current.ClassifierId);
                }
                current.Children.ForEach(x => stack.Push(x));
            }
            return classifierIds;
        }

        // called from root
        public List<Classifier> GetClassifiers()
        {
            List<ObjectId> classifierIds = this.GetClassifierIds();
            List<Classifier> result = new List<Classifier>();
            
            classifierIds.ForEach(cid => result.Add(Classifier.GetById(cid)));

            return result;

        }


        // called from root
        public InferenceData InferSingle(string originalText, string preProcessedText)
        {
            var result = new InferenceData() {
                RootId = this.Id, 
                PreProcessedText = preProcessedText, 
                Text = originalText
            };
            TrainedTaxonomy current = this;
            Classifier currentClassifier;
            InferenceStepData currentStepData;

            while (current.ClassifierId != default)
            {
                currentClassifier = Classifier.GetById(current.ClassifierId);
                currentStepData = currentClassifier.Infer(preProcessedText, 0.5);
                var next = current.Children.Where(x => x.Name == currentStepData.NextNode).FirstOrDefault();

                result.ThresholdHit.Add(currentStepData.ThresholdHit);
                result.PathToLeaf.Add(currentStepData.NextNode);
                result.LeafIds.Add(next.Id);
                current = next;
            }


            

            return result;
        }

        // called from root
        public void Infer(InferenceData input, double threshold)
        {
            TrainedTaxonomy current = this;
            Classifier currentClassifier;
            InferenceStepData currentStepData;

            while (current.ClassifierId != default)
            {
                currentClassifier = Classifier.GetById(current.ClassifierId);
                currentStepData = currentClassifier.Infer(input.PreProcessedText, threshold);
                var next = current.Children.Where(x => x.Name == currentStepData.NextNode).FirstOrDefault();

                input.ThresholdHit.Add(currentStepData.ThresholdHit);
                input.PathToLeaf.Add(currentStepData.NextNode);
                input.LeafIds.Add(next.Id);
                current = next;
            }

            for (var i = input.LeafIds.Count - 1; i >= 0; i--)
            {
                if (input.ThresholdHit[i])
                {
                    input.LastHit = input.LeafIds[i];
                    break;
                }
            }
        }


        public int ReviewCount(ObjectId testBulkId)
        {

            var query = from id in InferenceData.Collection.AsQueryable()
                        where id.LastHit == this.Id && id.TestBulkId == testBulkId
                        select id;
            return query.ToList().Count();
        }

        



    }
}
