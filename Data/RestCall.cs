﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NLog;
using RestSharp;
using System.Collections.Generic;

namespace Data
{
    public static class RestCall
    {

        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public static JsonSerializerSettings JsonSettings;
        static RestCall()
        {
            JsonSettings = new JsonSerializerSettings()
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
        }
        // Post
        public static string PostRaw(string url, object body)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);
            var bodyJson = JsonConvert.SerializeObject(body, JsonSettings);
            request.AddParameter("application/json", bodyJson, ParameterType.RequestBody);
            var resp = client.Execute(request);
            if (resp.IsSuccessful)
            {
                return resp.Content;
            }
            else if (resp.ContentType == "application/json")
            {
                var respbody = JsonConvert.DeserializeObject<Dictionary<string, object>>(resp.Content);
                _logger.Error($"Request to {url} was completed unsuccessfully with body: {bodyJson} and response: {resp.Content}");
                throw new RestCallException(resp.StatusCode, respbody);
            }
            _logger.Error($"Failed to communicate with server: {url} with body: {bodyJson} and statuscode:{resp.StatusCode} and response:{resp.Content}");
            throw new RestCallException(resp.StatusCode, resp.Content);
        }

        public static Dictionary<string, object> Post(string url, object body)
        {
            var res = PostRaw(url, body);
            var dic = JsonConvert.DeserializeObject<Dictionary<string, object>>(res);
            return dic;
        }

        public static T Post<T>(string url, object body)
        {
            var res = PostRaw(url, body);
            var dic = JsonConvert.DeserializeObject<T>(res);
            return dic;
        }

        public static string PostRaw(string url, object body, Dictionary<string, string> headers)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);
            var bodyJson = JsonConvert.SerializeObject(body);

            foreach (var item in headers)
            {
                request.AddHeader(item.Key, item.Value);
            }

            request.AddParameter("application/json", bodyJson, ParameterType.RequestBody);
            var resp = client.Execute(request);
            if (resp.IsSuccessful)
            {
                return resp.Content;
            }
            else if (resp.ContentType == "application/json")
            {
                var respbody = JsonConvert.DeserializeObject<Dictionary<string, object>>(resp.Content);
                _logger.Error($"Request to {url} was completed unsuccessfully with body: {bodyJson} and headers: {JsonConvert.SerializeObject(headers)} and response: {resp.Content}");
                throw new RestCallException(resp.StatusCode, respbody);
            }
            _logger.Error($"Failed to communicate with server: {url} with body: {bodyJson} and statuscode:{resp.StatusCode} and response:{resp.Content}");
            throw new RestCallException(resp.StatusCode, resp.Content);
        }

        public static Dictionary<string, object> Post(string url, object body, Dictionary<string, string> headers)
        {
            var res = PostRaw(url, body, headers);
            var dic = JsonConvert.DeserializeObject<Dictionary<string, object>>(res);
            return dic;
        }

        public static T Post<T>(string url, object body, Dictionary<string, string> headers)
        {
            var res = PostRaw(url, body, headers);
            var dic = JsonConvert.DeserializeObject<T>(res);
            return dic;
        }

        // Get
        public static string GetRaw(string url)
        {

            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            var resp = client.Execute(request);
            if (resp.IsSuccessful && resp.ContentType.StartsWith("application/json"))
            {
                return resp.Content;
            }
            else if (resp.ContentType == "application/json")
            {
                var respbody = JsonConvert.DeserializeObject<Dictionary<string, object>>(resp.Content);
                _logger.Error($"Request to {url} was completed unsuccessfully with response: {resp.Content}");
                throw new RestCallException(resp.StatusCode, respbody);
            }
            _logger.Error($"Failed to communicate \nwith server: {url}\nwith resp.ContentType: {resp.ContentType}\nwith resp.StatusCode: {resp.StatusCode}\nwith resp.Content: {resp.Content}");
            throw new RestCallException(resp.StatusCode, resp.Content);
        }

        public static T Get<T>(string url)
        {
            var res = GetRaw(url);
            var dic = JsonConvert.DeserializeObject<T>(res);
            return dic;
        }

        public static Dictionary<string, object> Get(string url)
        {
            var res = GetRaw(url);
            var dic = JsonConvert.DeserializeObject<Dictionary<string, object>>(res);
            return dic;
        }

        public static string GetRaw(string url, Dictionary<string, string> headers)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);

            foreach (var item in headers)
            {
                request.AddHeader(item.Key, item.Value);
            }

            var resp = client.Execute(request);
            if (resp.IsSuccessful && resp.ContentType.StartsWith("application/json"))
            {
                return resp.Content;
            }
            else if (resp.ContentType == "application/json")
            {
                var respbody = JsonConvert.DeserializeObject<Dictionary<string, object>>(resp.Content);
                _logger.Error($"Request to {url} was completed unsuccessfully with and headers: {JsonConvert.SerializeObject(headers)} and response: {resp.Content}");
                throw new RestCallException(resp.StatusCode, respbody);
            }
            _logger.Error($"Failed to communicate with server: {url} with statuscode:{resp.StatusCode} and response:{resp.Content}");
            throw new RestCallException(resp.StatusCode, resp.Content);
        }

        public static T Get<T>(string url, Dictionary<string, string> headers)
        {
            var res = GetRaw(url, headers);
            var dic = JsonConvert.DeserializeObject<T>(res);
            return dic;
        }

        public static Dictionary<string, object> Get(string url, Dictionary<string, string> headers)
        {
            var res = GetRaw(url, headers);
            var dic = JsonConvert.DeserializeObject<Dictionary<string, object>>(res);
            return dic;
        }

        // Patch
        public static string PatchRaw(string url, object body, Dictionary<string, string> headers = null)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.PATCH);
            var bodyJson = JsonConvert.SerializeObject(body);
            request.AddParameter("application/json", bodyJson, ParameterType.RequestBody);

            if (headers != null)
                foreach (var item in headers)
                {
                    request.AddHeader(item.Key, item.Value);
                }


            var resp = client.Execute(request);
            if (resp.IsSuccessful)
            {
                return resp.Content;
            }
            else if (resp.ContentType == "application/json")
            {
                var respbody = JsonConvert.DeserializeObject<Dictionary<string, object>>(resp.Content);
                _logger.Error($"Request to {url} was completed unsuccessfully with body: {bodyJson} and headers: {JsonConvert.SerializeObject(headers)} and response: {resp.Content}");
                throw new RestCallException(resp.StatusCode, respbody);
            }
            _logger.Error($"Failed to communicate with server: {url} with body: {bodyJson} and statuscode:{resp.StatusCode} and response:{resp.Content}");
            throw new RestCallException(resp.StatusCode, resp.Content);
        }

        public static Dictionary<string, object> Patch(string url, object body, Dictionary<string, string> headers = null)
        {
            var res = PatchRaw(url, body, headers);
            var dic = JsonConvert.DeserializeObject<Dictionary<string, object>>(res);
            return dic;
        }

        public static T Patch<T>(string url, object body, Dictionary<string, string> headers = null)
        {
            var res = PatchRaw(url, body, headers);
            var dic = JsonConvert.DeserializeObject<T>(res);
            return dic;
        }

        // Delete
        public static string DeleteRaw(string url, object body, Dictionary<string, string> headers = null)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.DELETE);
            var bodyJson = JsonConvert.SerializeObject(body);

            if (headers != null)
                foreach (var item in headers)
                {
                    request.AddHeader(item.Key, item.Value);
                }

            request.AddParameter("application/json", bodyJson, ParameterType.RequestBody);
            var resp = client.Execute(request);
            if (resp.IsSuccessful)
            {
                return resp.Content;
            }
            else if (resp.ContentType == "application/json")
            {
                var respbody = JsonConvert.DeserializeObject<Dictionary<string, object>>(resp.Content);
                _logger.Error($"Request to {url} was completed unsuccessfully with body: {bodyJson} and headers: {JsonConvert.SerializeObject(headers)} and response: {resp.Content}");
                throw new RestCallException(resp.StatusCode, respbody);
            }
            _logger.Error($"Failed to communicate with server: {url} with body: {bodyJson} and statuscode:{resp.StatusCode} and response:{resp.Content}");
            throw new RestCallException(resp.StatusCode, resp.Content);
        }

        public static Dictionary<string, object> Delete(string url, object body, Dictionary<string, string> headers = null)
        {
            var res = DeleteRaw(url, body, headers);
            var dic = JsonConvert.DeserializeObject<Dictionary<string, object>>(res);
            return dic;
        }

        public static T Delete<T>(string url, object body, Dictionary<string, string> headers = null)
        {
            var res = DeleteRaw(url, body, headers);
            var dic = JsonConvert.DeserializeObject<T>(res);
            return dic;
        }

        // Put
        public static string PutRaw(string url, object body, Dictionary<string, string> headers = null)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.PUT);
            var bodyJson = JsonConvert.SerializeObject(body);

            if (headers != null)
                foreach (var item in headers)
                {
                    request.AddHeader(item.Key, item.Value);
                }

            request.AddParameter("application/json", bodyJson, ParameterType.RequestBody);
            var resp = client.Execute(request);
            if (resp.IsSuccessful)
            {
                return resp.Content;
            }
            else if (resp.ContentType == "application/json")
            {
                var respbody = JsonConvert.DeserializeObject<Dictionary<string, object>>(resp.Content);
                _logger.Error($"Request to {url} was completed unsuccessfully with body: {bodyJson} and headers: {JsonConvert.SerializeObject(headers)} and response: {resp.Content}");
                throw new RestCallException(resp.StatusCode, respbody);
            }
            _logger.Error($"Failed to communicate with server: {url} with body: {bodyJson} and statuscode:{resp.StatusCode} and response:{resp.Content}");
            throw new RestCallException(resp.StatusCode, resp.Content);
        }

        public static Dictionary<string, object> Put(string url, object body, Dictionary<string, string> headers = null)
        {
            var res = PutRaw(url, body, headers);
            var dic = JsonConvert.DeserializeObject<Dictionary<string, object>>(res);
            return dic;
        }

        public static T Put<T>(string url, object body, Dictionary<string, string> headers = null)
        {
            var res = PutRaw(url, body, headers);
            var dic = JsonConvert.DeserializeObject<T>(res);
            return dic;
        }
    }
}
