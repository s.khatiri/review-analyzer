﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using NLog;

namespace Data
{
    public class Taxonomy
    {
        [BsonIgnore]
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        [BsonId]
        public ObjectId Id { get; set; }
        public List<string> Keywords { get; set; }
        public string Name { get; set; }
        public int TopicId { get; set; }
        
        public string ProcessName { get; set; }
        [BsonIgnoreIfNull]
        public ReviewStatus Status { get; set; } = ReviewStatus.NotAssigned;
        [BsonIgnore]
        public List<AppReview> Reviews { get; set; }
        [BsonIgnore]
        public int ReviewsCount { get; set; }
        public List<Taxonomy> Children { get; set; }
        public bool HasChildren
        {
            get
            {
                if (this.Children == null || !this.Children.Any())
                    return false;
                return true;
            }
        }
        public enum ReviewStatus
        {
            NotAssigned,
            Ok,
            Merged,
            Ignored
        }


        public Taxonomy()
        {
            Children = new List<Taxonomy>();
            Reviews = new List<AppReview>();
            Keywords = new List<string>();
        }



        public static IMongoCollection<Taxonomy> Collection
        {
            get => MongoDb.Database.GetCollection<Taxonomy>("Taxonomies");
        }

        public static Taxonomy GetById(ObjectId rootId, int reviewCount = 0)
        {
            var res = Collection.Find(x => x.Id == rootId);
            if (res.CountDocuments() <= 0)
            {
                return null;
            }
            var t = res.FirstOrDefault();
            if (reviewCount > 0)
            {
                t.Reviews = Review_Taxonomy.GetByTaxonomy(t.Id, reviewCount);
                t.ReviewsCount = t.Reviews.Count();
            }
            return t;
        } 

        public static Taxonomy GetGenericNode(string rootId, string taxonomyId)
        {
            var roid = ObjectId.Parse(rootId);
            var toid = ObjectId.Parse(taxonomyId);
            var root = GetById(roid);
            return root.SearchRecursive(toid);
        }

        

        public static List<Taxonomy> GetAll()
        {
            var res = Collection.Find(x => true);
            if (res.CountDocuments() <= 0)
            {
                return new List<Taxonomy>();
            }
            return res.ToList();
        }


        // THIS FUNCTION IS ONLY CALLED FROM ROOT OBJECT
        public void Save()
        {
            var existing = GetById(this.Id);
            this.GenerateChildrenIdRecursive();
            if (existing == null)
            {
                Collection.InsertOne(this);
            }
            else
            {
                var filter = Builders<Taxonomy>.Filter.Eq(doc => doc.Id, this.Id);
                Collection.ReplaceOne(filter, this);
            }
        }

        public void GenerateChildrenIdRecursive()
        {
            if (this.HasChildren)
            {
                foreach (var child in this.Children)
                {
                    if (child.Id == default)
                    {
                        child.Id = ObjectId.GenerateNewId();
                    }
                    child.GenerateChildrenIdRecursive();

                }
            }
        }

        // this function must be drived from the root object
        public Taxonomy SearchRecursive(ObjectId targetId)
        {
            if (this.Id == targetId)
            {
                return this;
            }

            if (this.HasChildren)
            {
                var res = this.Children.Where(x => x.Id == targetId).FirstOrDefault();
                if (res != null)
                {
                    return res;
                }
                else
                {
                    foreach (var child in this.Children)
                    {
                        res = child.SearchRecursive(targetId);
                        if (res != null)
                        {
                            return res;
                        }
                    }
                } 
            }
            return null;
        }

        public bool HasAssociatedReview()
        {

            return Review_Taxonomy.HasAssociatedReview(this.Id);
        }

        public long ReviewCount()
        {
            return Review_Taxonomy.ReviewCount(this.Id);
        } 


        public void AnnotateReviews(int reviewCount = 100)
        {
            this.Reviews = Review_Taxonomy.GetByTaxonomy(this.Id, reviewCount);
            this.ReviewsCount = this.Reviews.Count();
            _logger.Info("loaded {0} reviews for {1}", ReviewsCount, Name);
        }

        // this function must be drived from the root object
        public Taxonomy SearchAndRemoveRecursive(ObjectId targetId)
        {
            if (this.HasChildren)
            {
                var res = this.Children.Where(x => x.Id == targetId).FirstOrDefault();
                if (res != null)
                {
                    Children.Remove(res);
                    return res;
                }
                else
                {
                    foreach (var c in Children)
                    {
                        res = c.SearchAndRemoveRecursive(targetId);
                        if (res != null) return res;
                    }
                }
            }
            return null;
        }


        // this function must be called from the node which we want to find its parent
        public Taxonomy FindParent(Taxonomy root)
        {
            var res = root.FindParentRecursive(this.Id);
            return res;
        }

        public static Taxonomy FindParent(ObjectId rootId, ObjectId targetId)
        {
            var root = GetById(rootId);
            var res = root.FindParentRecursive(targetId);
            return res;
        }

        private Taxonomy FindParentRecursive(ObjectId targetId) 
        {
            if (this.HasChildren)
            {
                var res = this.Children.Where(x => x.Id == targetId).FirstOrDefault();
                if (res != null)
                {
                    return this;
                }
                else
                {
                    foreach (var child in this.Children)
                    {
                        res = child.FindParentRecursive(targetId);
                        if (res != null)
                        {
                            return res;
                        }
                    }
                }
            }
            return null;
        }

        // called from the node that we want to update its status
        public void UpdateStatus(ObjectId rootId, ReviewStatus newStatus)
        {
            this.Status = newStatus;
            var root = GetById(rootId);
            root.Save();
        }
        
        // called from the node that we want to update its status
        public void UpdateStatus(Taxonomy root, ReviewStatus newStatus)
        {
            this.Status = newStatus;
            root.Save();
        }

        // called from the node that we want to update its name
        public void UpdateName(Taxonomy root, string newName)
        {
            this.Name = newName;
            root.Save();
        }

        // called from the root
        public void DeleteSubtree(ObjectId subtree)
        {
            // this == root
            Taxonomy taxonomy = this.SearchRecursive(subtree);
            Taxonomy parent = taxonomy.FindParent(this);
            parent.Children.Remove(taxonomy);
            Stack<Taxonomy> stack = new Stack<Taxonomy>();
            Taxonomy current;
            stack.Push(taxonomy);

            while (stack.Count() > 0)
            {
                current = stack.Pop();
                Review_Taxonomy.DeleteByTaxonomy(current.Id);
                current.Children.ForEach(ch => stack.Push(ch));
            };

            this.Save();

        }

        

        public static List<string> GetClusters()
        {
            var names = Collection.AsQueryable().Select(x => x.ProcessName).Distinct().ToList();
            return names;
        }

        public static Taxonomy GetByCluster(string processName)
        {
            var res = Collection.Find(x => x.ProcessName == processName);
            return res.FirstOrDefault();
        }

        private static List<AppReview> GetSubtreeReviews(Taxonomy child,double threshold)
        {
            var result = new List<AppReview>();
            var leaves = new List<Taxonomy>();
            Stack<Taxonomy> stack = new Stack<Taxonomy>();
            stack.Push(child);
            Taxonomy current;

            while (stack.Count() > 0)
            {
                current = stack.Pop();
                //var reviews = Review_Taxonomy.GetByTaxonomy(current.Id);
                //result.AddRange(reviews);
                //foreach (var ch in current.Children)
                //{
                //    stack.Push(ch);
                //}
                if (!current.HasChildren)
                {
                    leaves.Add(current);
                }
                else
                {
                    current.Children.ForEach(x => { stack.Push(x); });
                }
            }
            //leaves.ForEach(x => {
            //    result.AddRange(Review_Taxonomy.GetByTaxonomy(x.Id));
            //});
            foreach (var leaf in leaves)
            {
                result.AddRange(Review_Taxonomy.GetByTaxonomy(leaf.Id,int.MaxValue,threshold));
            }
            return result;


        }

        public static TrainingData GetSubtreeReviews(Taxonomy root, ObjectId subtreeRootId, double threshold)
        {
            var result = new TrainingData();
            var taxonomy = root.SearchRecursive(subtreeRootId);
            var appReviewList = GetSubtreeReviews(taxonomy, threshold);
            appReviewList.ForEach(x =>
            {
                result.Texts.Add(x.PreProcessedText);
                result.Labels.Add(taxonomy.Name);
            });
            return result;
        }


        // only called by the root
        public void Train(string name,double threshold)
        {
            try
            {
                //_logger.Info("Preparing TraingData for{0}", name);
                _logger.Info("Preparing Classifiers for{0}", name);
                var trainedRoot = Clone(this);
                trainedRoot.Id = ObjectId.GenerateNewId();
                trainedRoot.TaxonomyRootId = this.Id;
                trainedRoot.ProcessName = name;
                trainedRoot.Name = name;


                Queue<TrainedTaxonomy> queue = new Queue<TrainedTaxonomy>();
                queue.Enqueue(trainedRoot);

                while (queue.Count() > 0)
                {
                    var current = queue.Dequeue();
                    //_logger.Info("preparing data for {0} subtree", current.Name);
                    if (current.HasChildren)
                    {
                        //TrainingData data = new TrainingData();
                        current.Children.ForEach(x =>
                        {
                            //_logger.Info("preparing data for {0} subtree", x.Name);
                            queue.Enqueue(x);
                            //var newData = GetSubtreeReviews(this, x.Id);
                            //data.Texts.AddRange(newData.Texts);
                            //data.Labels.AddRange(newData.Labels);
                        });
                        //data.Save();


                        _logger.Info("creating classifier for {0} subtree", current.Name);
                        var classifier = new Classifier()
                        {
                            Labels = current.Children.Select(x => x.Name).ToList(),
                            TaxonomyIds = current.Children.Select(x => x.Id).ToList(), 
                            TaxonomyRootId = this.Id,
                            Threshold=threshold
                            //TrainingDataId = data.Id
                        };
                        classifier.Save();
                        current.ClassifierId = classifier.Id;
                    }
                }

                _logger.Info("saving root for {0}", name);
                trainedRoot.Save();
            }
            catch(Exception e)

            {
                _logger.Error(e,"error while creating classifiers for {0}",name);
            }
        }

        private static TrainedTaxonomy Clone(Taxonomy root)
        {
            if (root == null)
            {
                return null;
            }
            var newNode = new TrainedTaxonomy()
            {
                Id = root.Id,
                Name = root.Name, 
                ProcessName = root.Name
            };

            foreach (var ch in root.Children)
            {
                newNode.Children.Add(Clone(ch));
            }
            return newNode;
        }


    }






}
