﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Data
{
    public class TestBulk
    {
        [BsonId]
        public ObjectId Id { get; set; }
        public ObjectId TrainedTaxonomyRootId { get; set; }
        public string ProcessName { get; set; }

        public static IMongoCollection<TestBulk> Collection
        {
            get => MongoDb.Database.GetCollection<TestBulk>("TestBulks");
        }

        public static TestBulk GetById(ObjectId Id)
        {
            var res = Collection.Find(x => x.Id == Id);
            if (res.CountDocuments() <= 0)
            {
                return null;
            }
            var t = res.FirstOrDefault();

            return t;
        }

        public void Save()
        {
            var existing = GetById(this.Id);
            if (existing == null)
            {
                Collection.InsertOne(this);
            }
            else
            {
                var filter = Builders<TestBulk>.Filter.Eq(doc => doc.Id, this.Id);
                Collection.ReplaceOne(filter, this);
            }
        }

        public List<InferenceData> GetInferenceData()
        {
            var query = from tb in Collection.AsQueryable()
                        join id in InferenceData.Collection.AsQueryable() on tb.Id equals id.TestBulkId
                        where this.Id == id.TestBulkId
                        select id;
            return query.ToList();
        }

        public static List<TestBulk> GetAll()
        {
            var res = Collection.Find(x => true);
            if (res.CountDocuments() <= 0)
            {
                return new List<TestBulk>();
            }
            return res.ToList();
        }
    }
}
