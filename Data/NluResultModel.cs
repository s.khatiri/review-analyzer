﻿using System.Collections.Generic;
using MongoDB.Bson;

namespace Data
{

    public class NluResultModel
    {
        // public string InputString;
        public List<PredictedIntent> IntentList { get; set; }
        public List<PredictedEntity> EntityList { get; set; }

        public NluResultModel()
        {
            IntentList = new List<PredictedIntent>();
            EntityList = new List<PredictedEntity>();
        }

        public NluResultModel(List<PredictedIntent> intentList, List<PredictedEntity> entityList)
        {
            IntentList = intentList;
            EntityList = entityList;
        }

        public class PredictedIntent
        {
            public ObjectId Id;
            public string IntentName;
            public double Confidence;
            public bool ThresholdHit;

            public PredictedIntent(string stringId, string name, double conf, bool thresholdHit)
            {
                Id = new ObjectId(stringId);
                Confidence = conf;
                ThresholdHit = thresholdHit;
                IntentName = name;
            }

            public PredictedIntent(string stringId, double conf, bool thresholdHit)
            {
                Id = new ObjectId(stringId);
                Confidence = conf;
                ThresholdHit = thresholdHit;
                IntentName = null;
            }

            public void LoadName()
            {
                //IntentName = Intent.GetById(Id).Name;
            }
        }

        public class PredictedEntity
        {
            public ObjectId Id { get; set; }
            public string EntityName { get; set; }
            public double Confidence { get; set; }

            public PredictedEntity(string stringId, string name, double conf)
            {
                Id = new ObjectId(stringId);
                EntityName = name;
                Confidence = conf;
            }

            public PredictedEntity(string stringId, double conf)
            {
                Id = new ObjectId(stringId);
                Confidence = conf;
                EntityName = null;
            }

            public void LoadName()
            {
                EntityName = null; // Entity.GetById(Id).Name;
            }
        }
    }

}
