﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data
{
    public class AppReview
    {
        
        public ObjectId Id { get; set; }
        public string ReviewId { get; set; }
        public string App { get; set; }
        public string Rate { get; set; }
        public string Title { get; set; }
        public string Review{ get; set; }
        public string PreProcessedText { get; set; }
        [BsonIgnore]
        public double Score { get; set; }
    public AppReview(string id,string app, string rate, string title,string review,string preprocessedText)
        {
            ReviewId = id;
            App = app;
            Rate = rate;
            Title = title;
            Review = review;
            PreProcessedText = preprocessedText;
        }
        public AppReview()
        {

        }

        // save current object into db
        public void Save()
        {
            var existing = Get(Id);
            if (existing == null)
                Collection.InsertOne(this);
            else
            {
                //this.Id = existing.Id;
                var filter = Builders<AppReview>.Filter.Eq(doc => doc.Id, this.Id);
                Collection.ReplaceOne(filter, this);
            }
        }

        // static: insert given AppReview s
        public static void InsertAll(List<AppReview> reviews)
        {
            Collection.InsertMany(reviews);
        }

        // static: get all apps which have AppReview s
        public static List<string> GetApps()
        {
            var res = Collection.AsQueryable().Select(x => x.App).Distinct().ToList() ;
            return res;
        }

        // static: get all AppReview s
        public static List<AppReview> Get()
        {
            var res = Collection.Find(x=> x.PreProcessedText != "");
            if (res.CountDocuments() <= 0)
                return new List<AppReview>();
            return res.ToList();
        }

        // static: get AppReviews corresponding to given apps
        public static List<AppReview> Get(List<string> apps)
        {
            var list = new List<AppReview>();
            foreach (var app in apps)
            {
                var res = Collection.Find(x => x.App==app && x.PreProcessedText!="");
                if (res.CountDocuments() > 0)
                    list.AddRange(res.ToList());
            }
            return list;
        }

        // static: get AppReview by id
        public static AppReview Get(ObjectId Id)
        {
            var res = Collection.Find(x => x.Id==Id);
            if (res.CountDocuments() <= 0)
                return null;
            return res.FirstOrDefault();
        }

        // mongo collection object
        public static IMongoCollection<AppReview> Collection
        {
            get => MongoDb.Database.GetCollection<AppReview>("AppReviews");
        }
    }


}
