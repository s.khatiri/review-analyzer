﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data
{
    public static class MongoDb
    {
        public readonly static string Ip = "localhost";
        public readonly static string Port = "27017";
        public readonly static MongoClient Client;
        public readonly static string DatabaseName = "AppReviewAnalyzer";

        static MongoDb()
        {
            var mongo = Environment.GetEnvironmentVariable("MONGODB");
            if (!string.IsNullOrEmpty(mongo))
                Ip = mongo;
            var dbName = Environment.GetEnvironmentVariable("MONGODB_DB");
            if (!string.IsNullOrEmpty(dbName))
                DatabaseName = dbName;
            var username = Environment.GetEnvironmentVariable("MONGODB_USERNAME") ?? "";
            var password = Environment.GetEnvironmentVariable("MONGODB_PASSWORD") ?? "";

            string connection = "";
            if (string.IsNullOrEmpty(username))
            {
                connection = string.Format("mongodb://{0}:{1}", Ip, Port);
            }
            else
            {
                connection = string.Format("mongodb://{0}:{1}@{2}:{3}", username, password, Ip, Port);
            }

            Client = new MongoClient(connection);

            // ConventionRegistry.Register("Ignore null values",
            //     new ConventionPack
            //     {
            //         new IgnoreIfNullConvention(true)
            //     },
            //     t => true);
            ConventionRegistry.Register("enums as string",
                new ConventionPack
                {
                    new EnumRepresentationConvention(BsonType.String)
                },
                t => true);

        }

        public static IMongoDatabase Database
        {
            get => MongoDb.Client.GetDatabase(DatabaseName);
        }
    }
}
