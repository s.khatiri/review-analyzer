﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data
{
    public class NluInferenceOutput
    {
        public List<Topic> topics;
        public List<Entity> entities;
    }

    public class Topic
    {
        public string itemId { get; set; }
        public double confidence { get; set;  }
        public bool thresholdHit { get; set; }
    }

    public class Entity
    {

    }
}
