﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Data
{
    public class InferenceData
    {   
        [BsonId]
        public ObjectId Id { get; set; }
        //public ObjectId InputId { get; set; }
        public ObjectId RootId { get; set; }
        public ObjectId TestBulkId { get; set; }
        public string Text { get; set; }
        public string PreProcessedText { get; set; }
        public List<ObjectId> LeafIds { get; set; }
        public List<string> PathToLeaf { get; set; }
        public List<bool> ThresholdHit { get; set; }
        public ObjectId LastHit { get; set; }
        public ObjectId ActualLabelId { get; set; }
        public string ActualLabel { get; set; }

        public InferenceData()
        {
            LeafIds = new List<ObjectId>();
            PathToLeaf = new List<string>();
            ThresholdHit = new List<bool>();
        }

        public static IMongoCollection<InferenceData> Collection
        {
            get => MongoDb.Database.GetCollection<InferenceData>("InferenceDatas");
        }

        public static InferenceData GetById(ObjectId Id)
        {
            var res = Collection.Find(x => x.Id == Id);
            if (res.CountDocuments() <= 0)
            {
                return null;
            }
            var t = res.FirstOrDefault();

            return t;
        }

        public void Save()
        {
            var existing = GetById(this.Id);
            if (existing == null)
            {
                Collection.InsertOne(this);
            }
            else
            {
                var filter = Builders<InferenceData>.Filter.Eq(doc => doc.Id, this.Id);
                Collection.ReplaceOne(filter, this);
            }
        }

        public static void InsertAll(List<InferenceData> many)
        {
            Collection.InsertMany(many);
        }

        public static List<InferenceData> GetByTrainedTaxonomy(ObjectId trainedTaxonomyId, ObjectId testBulkId, int count = 100)
        {
            
            var query = from id in Collection.AsQueryable()
                        where id.LastHit == trainedTaxonomyId && id.TestBulkId == testBulkId
                        select id;
            return query.Take(count).ToList();
        }
    }

    public class InferenceStepData
    {
        public bool ThresholdHit { get; set; }
        public string NextNode { get; set; }
    }

}
