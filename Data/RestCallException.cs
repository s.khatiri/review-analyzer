﻿using System;
using System.Net;

namespace Data
{
    public class RestCallException : Exception
    {
        public object Body;
        public HttpStatusCode StatusCode;
        public RestCallException(HttpStatusCode statusCode, object body)
        {
            Body = body;
            StatusCode = statusCode;
        }
    }
}
