﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;

namespace Data
{
    public class TrainingData
    {

        public List<string> Texts { get; set; }
        public List<string> Labels { get; set; }


        public TrainingData()
        {
            Texts = new List<string>();
            Labels = new List<string>();
        }
    }
    public class Classifier
    {
        [BsonId]
        public ObjectId Id { get; set; }
        public ObjectId TaxonomyRootId { get; set; }
        public List<ObjectId> TaxonomyIds { get; set; }
        public List<string> Labels { get; set; }
       
        public double Threshold { get; set; }
        [BsonIgnore]
        public string CustomCode { get => Id.ToString();}
        public string Status { get; set; }
       
        public Classifier()
        {
            Labels = new List<string>();
            TaxonomyIds = new List<ObjectId>();
            Status = "NOT_FOUND";
        }

        public static IMongoCollection<Classifier> Collection
        {
            get => MongoDb.Database.GetCollection<Classifier>("Classifiers");
        }
        public static Classifier GetById(ObjectId rootId)
        {
            var res = Collection.Find(x => x.Id == rootId);
            if (res.CountDocuments() <= 0)
            {
                return null;
            }
            var t = res.FirstOrDefault();
            
            return t;
        }

        public static List<Classifier> GetAll(bool loadTrainingData = true)
        {
            var res = Collection.Find(x => true);
            if (res.CountDocuments() <= 0)
                return new List<Classifier>();
            var resList = res.ToList();
            
            return resList;
        }

        
        public void Save()
        {
            var existing = GetById(this.Id);
            if (existing == null)
            {
                Collection.InsertOne(this);
            }
            else
            {
                var filter = Builders<Classifier>.Filter.Eq(doc => doc.Id, this.Id);
                Collection.ReplaceOne(filter, this);
            }
        }

        public TrainingData LoadTrainingData()
        {
            TrainingData result = new TrainingData();
            var root = Taxonomy.GetById(this.TaxonomyRootId);
            for (var i = 0; i < Labels.Count(); i++)
            {
                var currentTaxonomy = TaxonomyIds[i];
                var currentTrainingData = Taxonomy.GetSubtreeReviews(root, currentTaxonomy,Threshold);
                result.Texts.AddRange(currentTrainingData.Texts);
                result.Labels.AddRange(currentTrainingData.Labels);
            }

            return result;

        }

        public bool TrainingInProgress()
        {
            return NluUnitFacade.BuildNluModelStatus(this.CustomCode)["training_status"].ToString() == "TRAINING";
        }

        public InferenceStepData Infer(string PrePocessedText, double threshold)
        {
            var result = NluUnitFacade.GetInferenceResult(this.CustomCode, PrePocessedText, threshold);
            return new InferenceStepData()
            {
                ThresholdHit = result.topics.First().thresholdHit, 
                NextNode = result.topics.First().itemId
            };
        }
       


    }
}
