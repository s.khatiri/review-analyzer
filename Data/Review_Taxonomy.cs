﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;


namespace Data
{
    // each element in review-topic matrix is illustrated with one Review_Taxonomy object
    public class Review_Taxonomy
    {
        [BsonId]
        public ObjectId Id { set; get; }
        public ObjectId ReviewId { set; get; } // row in matrix
        //public ObjectId TopicId { set; get; } // col in matrix
        public ObjectId TaxonomyNodeId { get; set; }
        public ObjectId TaxonomyRootId { get; set; }
        [BsonIgnore]
        public AppReview Review { set; get; } // derived from ReviewId
        //[BsonIgnore]
        //public Topic Topic { set; get; } // derived from TopicId
        [BsonIgnore]
        public Taxonomy TaxonomyNode { get; set; }
        [BsonIgnore]
        public Taxonomy TaxonomyRoot { get; set; }

        public float Score { set; get; } // matrix element actual value
        //public static void MergeTopics(ObjectId source,ObjectId destination)
        //{
        //    //should handle intersections
        //    // Source TopicReviews stay in the db, new instances are created and their TopicId value is set to destination (instead of source)
        //    var sourcereviews = Collection.AsQueryable().Where(x => x.Taxonom == source).ToList();

        //    // Id is set to default so that intersection does not happen in the database (can't add 2 documents with equal ids)
        //    sourcereviews.ForEach(x => { x.TopicId = destination; x.Id = default; } );
        //    Collection.InsertMany(sourcereviews);
        //}
        public void Save()
        {
            if (ReviewId == default && Review != null)
                ReviewId = Review.Id;
            if (TaxonomyNodeId == default && TaxonomyNode != null)
                TaxonomyNodeId = TaxonomyNode.Id;
            if (TaxonomyRootId == default && TaxonomyRoot != null)
                TaxonomyRootId = TaxonomyRoot.Id;

            var existing = Get(Id);
            if (existing == null)
                Collection.InsertOne(this);
            else
            {
                //this.Id = existing.Id;
                var filter = Builders<Review_Taxonomy>.Filter.Eq(doc => doc.Id, this.Id);
                Collection.ReplaceOne(filter, this);
            }
        }

        public static bool HasAssociatedReview(ObjectId taxonomyNodeId)
        {
            var res = Collection.Find(x => x.TaxonomyNodeId == taxonomyNodeId);
            long val = res.CountDocuments();
            return val > 0;
        }

        public static long ReviewCount(ObjectId taxonomyNodeId)
        {
            var res = Collection.Find(x => x.TaxonomyNodeId == taxonomyNodeId);
            long val = res.CountDocuments();
            return val;
        }


        public static void InsertAll(List<Review_Taxonomy> review_Topics)
        {
            review_Topics.ForEach(x =>
            {
                if (x.ReviewId == default && x.Review != null)
                    x.ReviewId = x.Review.Id;
                if (x.TaxonomyNodeId == default && x.TaxonomyNode != null)
                    x.TaxonomyNodeId = x.TaxonomyNode.Id;
                if (x.TaxonomyRootId == default && x.TaxonomyRoot != null)
                    x.TaxonomyRootId = x.TaxonomyRoot.Id;
            });
            Collection.InsertMany(review_Topics);
        }
        public static List<Review_Taxonomy> Get()
        {
            var res = Collection.Find(x => true);
            if (res.CountDocuments() <= 0)
                return new List<Review_Taxonomy>();
            res.ForEachAsync(x =>
            {
                x.TaxonomyRoot = Taxonomy.GetById(x.TaxonomyRootId);
                x.TaxonomyNode = x.TaxonomyRoot.SearchRecursive(x.TaxonomyNodeId);
                x.Review = AppReview.Get(x.ReviewId);
            });

            
            return res.ToList();
        }
        public static Review_Taxonomy Get(ObjectId Id)
        {
            var res = Collection.Find(x => x.Id == Id);
            if (res.CountDocuments() <= 0)
                return null;

            return res.FirstOrDefault();
        }

        public static List<AppReview> GetByTaxonomy(ObjectId taxonomyId, int count = int.MaxValue, double threshold = 0)
        {
            var query = from rt in Collection.AsQueryable(new AggregateOptions { AllowDiskUse = true })
                        join r in AppReview.Collection.AsQueryable() on rt.ReviewId equals r.Id
                        where rt.TaxonomyNodeId == taxonomyId && rt.Score >= threshold
                        select new AppReview()
                        {
                            App=r.App,
                            Id=r.Id,
                            Rate=r.Rate,
                            Review=r.Review,
                            ReviewId=r.ReviewId,
                            Score=rt.Score,
                            Title=r.Title,
                            PreProcessedText = r.PreProcessedText
                        };
            
            return query.OrderByDescending(x=>x.Score).Take(count).ToList();
        }

        public static List<Review_Taxonomy> GetRtsByTaxonomy(ObjectId taxonomyId)
        {
            var query = from rt in Collection.AsQueryable()
                        where rt.TaxonomyNodeId == taxonomyId
                        select rt;
            return query.ToList();
        }

        public static List<ObjectId> GetReviewIds(ObjectId taxonomyId)
        {

            var res = Collection.Find(x => x.TaxonomyNodeId == taxonomyId);
            if (res.CountDocuments() <= 0)
                return new List<ObjectId>();
            var ret = res.ToList().Select(x => x.ReviewId).ToList();
            return ret;
        }

        public static int GetTaxonomyCount(ObjectId taxonomyId)
        {
            var res = Collection.Find(x => x.TaxonomyNodeId == taxonomyId).CountDocuments();
            return (int)res;
        }


        public static void DeleteByTaxonomy(ObjectId taxonomyId)
        {
            Collection.DeleteMany(x => x.TaxonomyNodeId == taxonomyId);
        }

        public static IMongoCollection<Review_Taxonomy> Collection
        {
            get => MongoDb.Database.GetCollection<Review_Taxonomy>("Review_Taxonomies");
        }

    }
}
