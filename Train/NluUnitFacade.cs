﻿//using System.Collections.Generic;
//using System;
//using NLog;
//using Data;
//using System.Linq;

//namespace Train
//{
//    public class NluUnitFacade
//    {
//        private static Logger _logger = LogManager.GetCurrentClassLogger();
//        // public static string NluURL = "http://localhost:8080";
//        public static string NluURL = "http://nlu.textapi.ir";
//        public static string ClassifierInferenceURL => NluURL + "/inference/single?custom_code={0}";
//        public static string ClassifierTrainURL => NluURL + "/train/bulk?custom_code={0}";
//        public static string ClassifierTrainConfigURL => NluURL + "/train/config?custom_code={0}";
//        public static string ClassifierTrainStatusURL => NluURL + "/train/status?custom_code={0}";
//        public static string ClassifierEstimatURL => NluURL + "/evaluation/bulk";
//        public static string ClassifierEstimatStatusURL => NluURL + "/evaluation/status";

//        static NluUnitFacade()
//        {
//            var url = Environment.GetEnvironmentVariable("NLU_UNIT_API");
//            if (!string.IsNullOrEmpty(url))
//                NluURL = url;
//        }

//        public static NluResultModel GetInferenceResult(string classifierId,string preprocessedText, double threshold)
//        {
//            var url = string.Format(ClassifierInferenceURL, classifierId);
//            var payload = new InferenceInputModel(preprocessedText, threshold);
//            var resp = RestCall.Post<InferenceOutputModel>(url, payload);
//            return resp.ToNluResultModel();
//        }

//        public static Dictionary<string, object> TrainClassifier(Classifier classifier)
//        {
//            var trainingData = new TrainParameter.TrainingDataset(classifier.LoadTrainingData());
//            return BuildNluModel(classifier.CustomCode, trainingData, (float)0.5);
//        }

//        public static Dictionary<string, object> BuildNluModel(string customCode, TrainParameter.TrainingDataset traingingData, float trheshold)
//        {

//            var url = string.Format(ClassifierTrainURL, customCode);

//            var payload = new { confidenceThreshold = trheshold, trainingData = traingingData};

//            var res = RestCall.Post(url, payload);
//            return res;  // new Dictionary<string, object>();
//        }

//        public static Dictionary<string, object> BuildNluModelStatus(string customCode)
//        {
//            var url = string.Format(ClassifierTrainURL, customCode);
//            var res = RestCall.Get(url);
//            return res;
//        }

//        public static Dictionary<string, object> GetTrainConfig(string customCode)
//        {
//            var url = string.Format(ClassifierTrainConfigURL, customCode);
//            var res = RestCall.Get(url);
//            return res;
//        }

//        public static Dictionary<string, object> SetTrainConfig(string customCode, List<Dictionary<string, object>> configList)
//        {
//            var url = string.Format(ClassifierTrainConfigURL, customCode);
//            var payload = new TrainConfig(configList);
//            var res = RestCall.Post(url, payload);
//            return res;
//        }


//        private class InferenceInputModel
//        {
//            public string Input;
//            public double Threshold;

//            public InferenceInputModel(string stemedText, double threshold)
//            {
//                Input = stemedText;
//                Threshold = threshold;
//            }
//        }

//        public class InferenceOutputModel
//        {
//            public List<NluItem> Topics { get; set; }
//            public List<NluItem> Entities { get; set; }

//            public NluResultModel ToNluResultModel()
//            {
//                var res = new NluResultModel();
//                foreach (var item in this.Topics)
//                {
//                    var intent = new NluResultModel.PredictedIntent(item.ItemId, item.Confidence, item.ThresholdHit);
//                    intent.LoadName();
//                    res.IntentList.Add(intent);
//                }
//                foreach (var item in this.Entities)
//                {
//                    var entity = new NluResultModel.PredictedEntity(item.ItemId, item.Confidence);
//                    entity.LoadName();
//                    res.EntityList.Add(entity);
//                }
//                return res;
//            }
//            public class NluItem
//            {
//                public string ItemId;
//                public double Confidence;
//                public bool ThresholdHit;
//            }
//        }

//        public class TrainConfig
//        {
//            public List<Dictionary<string, Object>> ClassifierParams;

//            public TrainConfig(List<Dictionary<string, Object>> classifierParams)
//            {
//                ClassifierParams = classifierParams;
//            }
//        }

//        public class TrainParameter
//        {
//            public float ConfidenceThreshold;
//            public TrainingDataset TrainingData;

//            //public TrainParameter(List<Dialog.Data.Models.Mongo.Intents.Intent> traingingData, float classifierThreshold)
//            //{
//            //    ConfidenceThreshold = classifierThreshold;
//            //    TrainingData = new TrainingDataset(traingingData);
//            //}

//            public class TrainingDataset
//            {
//                public List<string> data;
//                public List<string> tags;

//                public TrainingDataset(TrainingData td)
//                {
//                    data = td.Texts;
//                    tags = td.Labels;
//                }
//            }
//        }
//    }
//}
